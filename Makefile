all: servidor cliente

servidor: obj/servidor.o
	gcc -Wall -g $^ -o bin/$@

cliente: obj/cliente.o 
	gcc -Wall -g $^ -o bin/$@

obj/servidor.o: src/servidor.c
	gcc -c $^ -o $@

obj/cliente.o: src/cliente.c 
	gcc -c $^ -o $@

.PHONY: clean

clean:
	rm -rf obj/* bin/*
